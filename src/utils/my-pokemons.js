import { isClient } from './common'
export const getMyPokemons = () => {
  if (!isClient) {
    return []
  }

  const savedPokemons = window.localStorage.getItem('my_pokemons')

  return savedPokemons ? JSON.parse(savedPokemons) : []
}

export const savePokemon = ({ name, nickName, image }) => {
  if (!isClient) {
    return false
  }

  const myPokemons = getMyPokemons()

  const saveData = [
    {
      name,
      nickName,
      image
    },
    ...myPokemons
  ]

  window.localStorage.setItem('my_pokemons', JSON.stringify(saveData))
  window.dispatchEvent(new Event('storage'))

  return true
}

export const releasePokemon = ({ index }) => {
  const myPokemons = getMyPokemons()
  const removed = myPokemons.splice(index, 1)

  window.localStorage.setItem('my_pokemons', JSON.stringify(myPokemons))
  window.dispatchEvent(new Event('storage'))

  return true
}
