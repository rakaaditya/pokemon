export const isClient = typeof window !== 'undefined'
export const formatPokemonName = (name = '') => `${name.slice(0, 1).toUpperCase()}${name.slice(1, name.length)}`
export const formatAbilities = (abilities = []) => abilities.filter(ability => !ability.is_hidden).map(ability => ability.ability.name).join(', ')
