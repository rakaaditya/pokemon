import styled from '@emotion/styled'
import { css } from '@emotion/react'

const Input = styled.input`
  border: none;
  border-radius: 8px;
  background-color: #F7F7F7;
  padding: 12px;
  font-weight: 500;
  font-size: 16px;

  &::placeholder {
    color: #9DA3B4;
  }

  &:focus {
    outline: none;
  }

  ${props => props.block && css`
    display: block;
    width: 100%;
  `}
`

export default Input
