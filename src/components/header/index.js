import { HeaderNav } from './styles'
import router from 'next/router'
import Link from 'next/link'
import Image from 'next/image'

export default function Header({ title, home, withBackButton }) {
  const handleBack = () => {
    router.back()
  }

  return (
    <HeaderNav>
      <div className='wrapper'>
        {home
          ? (
            <div className='home'>
              <Link href='/'>
                <a>
                  <Image
                    src={require('@images/logo.png')}
                    width={86}
                    height={32}
                    alt='Pokemon Logo'
                  />
                </a>
              </Link>
              <Link href='/my-pokemons'>
                <a>
                  My Pokemon
                </a>
              </Link>
            </div>
            )
          : (
            <div className='title'>{title}</div>
            )}
        {withBackButton && (
          <div className='back'>
            <button onClick={handleBack}>
              <Image
                src={require('@images/icons/back-arrow.svg')}
                width={16}
                height={16}
                alt='Back'
              />
            </button>
          </div>
        )}
      </div>
    </HeaderNav>
  )
}
