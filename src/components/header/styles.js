import styled from '@emotion/styled'

export const HeaderNav = styled.nav`
  position: fixed;
  width: 100vw;
  left: 0;
  top: 0;
  background: #FFFFFF;
  box-shadow: 0px 2px 8px 4px rgba(67, 72, 85, 0.05);
  height: 52px;
  z-index: 2;
  padding: 8px 16px;

  .wrapper {
    position: relative;
    height: 100%;

    img {
      cursor: pointer;
    }

    .title {
      text-align: center;
      display: flex;
      align-items: center;
      justify-content: center;
      font-weight: 700;
      font-size: 16px;
      color: #3E4148;
      height: 100%;
    }

    .home {
      width: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;

      a {
        &:last-child {
          text-decoration: none;
          color: #FFA21F;
          font-weight: bold;
          font-size: 14px;
        }
      }
    }

    .back {
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      button {
        background: none;
        border: none;
      }
    }

  }

`
