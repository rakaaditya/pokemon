import styled from '@emotion/styled'

export const H1 = styled.h1`
  color: #3E4148;
  font-size: 24px;
  font-weight: 700;
  line-height: 36px;
`
