import { css } from '@emotion/react'
import styled from '@emotion/styled'

const Card = styled.div`
  border-radius: 8px;
  box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.05);
  padding: 12px;

  ${props => props.flat && css`
    box-shadow: none;
    border: 1px solid #F5F5F7;
  `}
`

export default Card
