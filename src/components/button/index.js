import styled from '@emotion/styled'
import { css } from '@emotion/react'

const Button = styled.button`
  font-family: 'Poppins';
  background: none;
  border: none;
  padding: 12px;
  font-size: 16px;
  font-weight: bold;
  border-radius: 8px;
  border: 1px solid;
  cursor: pointer;

  ${props => props.primary && css`
    color: #FFFFFF;
    background-color: #FFA21F;
  `}

  ${props => props.secondary && css`
    color: #DE561C;
    border-color: #DE561C;
  `}

  ${props => props.block && css`
    display: block;
    width: 100%;
  `}

  ${props => props.text && css`
    color: #9DA3B4;
    background-color: none;
    border: none;
  `}
`

export default Button
