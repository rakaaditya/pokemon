import styled from '@emotion/styled'

export const Main = styled.main`
  position: relative;
  padding: 24px 16px;
  padding-top: 75px;
  max-width: 375px;
  margin: 0 auto;
  min-height: 100vh;
`
