import Head from 'next/head'
import { useRouter } from 'next/router'
import Header from '@components/header'
import { Main } from './styles'

const Layout = ({ title = '', children }) => {
  const { pathname } = useRouter()

  const isHome = pathname === '/'

  return (
    <Main>
      <Head>
        <title>{`${title} - Pokemon`}</title>
      </Head>
      <Header title={title} home={isHome} withBackButton={!isHome} />
      {children}
    </Main>
  )
}

export default Layout
