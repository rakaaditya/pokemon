import MyPokemonsView from '@features/my-pokemons/views/my-pokemons'

export default function MyPokemonsPage () {
  return <MyPokemonsView />
}
