import emotionReset from 'emotion-reset'
import { ApolloProvider } from '@apollo/client'
import { Global, css } from '@emotion/react'
import apolloClient from '@config/apollo-client'
import { Provider as ToastProvider } from '@happyfresh/toast'
import MyPokemonsProvider from '@features/my-pokemons/data/provider'

function PokemonApp ({ Component, pageProps }) {
  return (
    <ApolloProvider client={apolloClient}>
      <Global styles={css`
        ${emotionReset}

        body {
          font-family: 'Poppins';
        }

        *, *::after, *::before {
          box-sizing: border-box;
          -moz-osx-font-smoothing: grayscale;
          -webkit-font-smoothing: antialiased;
          font-smoothing: antialiased;
        }
      `}
      />
      <ToastProvider>
        <MyPokemonsProvider>
          <Component {...pageProps} />
        </MyPokemonsProvider>
      </ToastProvider>
    </ApolloProvider>
  )
}

export default PokemonApp
