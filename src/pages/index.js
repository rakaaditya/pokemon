import PokemonListView from '@features/pokemon-list/views/pokemon-list'

export default function Home () {
  return <PokemonListView />
}
