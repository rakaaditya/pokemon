import PokemonDetailView from '@features/pokemon-detail/views/pokemon-detail'
import PokemonDetailProvider from '@features/pokemon-detail/data/provider'

export default function PokemonDetailPage () {
  return (
    <PokemonDetailProvider>
      <PokemonDetailView />
    </PokemonDetailProvider>
  )
}
