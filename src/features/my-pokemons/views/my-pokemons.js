import dynamic from 'next/dynamic'
import Layout from '@components/layout'
import useMyPokemons from '../data/hooks'

const MyPokemons = dynamic(
  () => import('../components/my-pokemons'),
  { ssr: false }
)

const EmptyStates = dynamic(
  () => import('../components/empty-states'),
  { ssr: false }
)

export default function MyPokemonsView () {
  const { pokemons } = useMyPokemons()

  const ChildComponent = pokemons.length ? MyPokemons : EmptyStates

  return (
    <Layout title='My Pokemons'>
      <ChildComponent />
    </Layout>
  )
}
