import { useState, useEffect } from 'react'
import Context from './context'
import { getMyPokemons, savePokemon, releasePokemon } from '@utils/my-pokemons'

export default function MyPokemonsProvider ({ children }) {
  const [pokemons, setPokemons] = useState([])

  useEffect(() => {
    setPokemons(getMyPokemons())
  }, [])

  useEffect(() => {
    window.addEventListener('storage', () => {
      setPokemons(getMyPokemons())
    })

    return function cleanup () {
      window.removeEventListener('storage', null)
    }
  }, [])

  const getOwnedCount = (name) => pokemons.filter(pokemon => pokemon.name === name).length

  return (
    <Context.Provider value={{
      pokemons,
      savePokemon,
      releasePokemon,
      getOwnedCount
    }}
    >
      {children}
    </Context.Provider>
  )
}
