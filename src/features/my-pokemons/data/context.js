import React from 'react'

export default React.createContext({
  pokemons: [],
  savePokemon: () => null,
  getOwnedCount: () => null,
  releasePokemon: () => null
})
