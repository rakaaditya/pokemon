import { useContext } from 'react'
import Context from './context'

export default function MyPokemonsHooks () {
  const ctx = useContext(Context)

  return ctx
}
