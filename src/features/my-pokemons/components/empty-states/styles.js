import styled from '@emotion/styled'

export const EmptyStatesContainer = styled.div`
  height: calc(100vh - 100px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .text {
    font-weight: bold;
    font-size: 16px;
    color: #9DA3B4;
    margin-top: 40px;
  }

  a {
    text-decoration: none;
    font-weight: bold;
    font-size: 16px;
    color: #FFFFFF;
    background-color: #FFA21F;
    border-radius: 8px;
    padding: 12px;
    text-align: center;
    width: 100%;
    margin-top: 120px;
  }
`
