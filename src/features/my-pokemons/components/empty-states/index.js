import Link from 'next/link'
import Image from 'next/image'
import { EmptyStatesContainer } from './styles'

export default function EmptyStates () {
  return (
    <EmptyStatesContainer>
      <Image src={require('@images/empty-my-pokemons.png')} width={210} height={154} alt='Empty' />
      <div className='text'>You haven’t caught any Pokemon:(</div>
      <Link href='/'>
        <a>Catch Pokemon now</a>
      </Link>
    </EmptyStatesContainer>
  )
}
