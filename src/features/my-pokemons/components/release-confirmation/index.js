import Image from 'next/image'
import { BottomSheet } from '@happyfresh/bottom-sheet'
import Button from '@components/button'
import { ReleaseConfirmationContainer } from './styles'

export default function ReleaseConfirmation ({ open, pokemon, onConfirm, onDismiss }) {
  return (
    <BottomSheet
      open={open} styles={{
        wrapper: {
          borderRadius: '16px 16px 0 0',
          height: 'unset',
          padding: '32px 16px 24px 16px'
        }
      }}
    >
      <ReleaseConfirmationContainer>
        <figure>
          <Image src={pokemon.image} width={100} height={100} alt='Release pokemon' />
        </figure>
        <div className='message'>Are you sure you want to release {pokemon.nickName}?</div>
        <Button primary block onClick={onDismiss}>No I’ll keep it!</Button>
        <Button text block onClick={onConfirm}>yes, release:(</Button>
      </ReleaseConfirmationContainer>
    </BottomSheet>
  )
}
