import styled from '@emotion/styled'

export const ReleaseConfirmationContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .message {
    font-weight: bold;
    font-size: 20px;
    color: #3E4148;
    padding: 0 16px;
    padding-top: 16px;
    padding-bottom: 24px;
    text-align: center;
    line-height: 30px;
  }

  button {
    &:last-child {
      margin-top: 8px;
    }
  }
`
