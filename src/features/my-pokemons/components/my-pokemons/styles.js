import styled from '@emotion/styled'

export const MyPokemonsSection = styled.section`
  ul {
    list-style: none;
    padding: 0;
    padding-bottom: 40px;

    li {
      margin: 8px 0;
    }
  }
`
