import MyPokemonCard from '../my-pokemon-card'
import useMyPokemons from '../../data/hooks'
import { MyPokemonsSection } from './styles'

export default function MyPokemons () {
  const { pokemons } = useMyPokemons()

  return (
    <MyPokemonsSection>
      <ul>
        {pokemons.map((pokemon, i) => (
          <li key={i}>
            <MyPokemonCard pokemon={pokemon} index={i} />
          </li>
        ))}
      </ul>
    </MyPokemonsSection>
  )
}
