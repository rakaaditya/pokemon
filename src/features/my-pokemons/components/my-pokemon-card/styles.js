import { css } from '@emotion/react'

export const card = css`
  position: relative;
  padding: 12px 16px;
  display: flex;
  align-items: center;

  figure {
    margin-right: 16px;
  }

  .info {
    .nickname {
      font-weight: bold;
      font-size: 16px;
      color: #3E4148;
      padding-bottom: 2px;
    }
    .name {
      font-weight: 600;
      font-size: 14px;
      color: #9DA3B4;
      padding-top: 2px;
    }
  }

  .action {
    position: absolute;
    top: 0;
    right: 16px;
    height: 100%;

    .wrapper {
      position: relative;
      display: flex;
      flex-direction: column;
      justify-content: center;
      height: 100%;
      align-items: flex-end;

      .menu-toggle {
        background: none;
        border: none;
        content: '';
        background-image: url('/images/icons/more.svg');
        background-repeat: no-repeat;
        width: 20px;
        height: 20px;
        cursor: pointer;
      }

      ul {
        position: absolute;
        top: 60px;
        right: 12px;

        &.hide {
          display: none;
        }

        li {
          button {
            cursor: pointer;
            border: none;
            display: flex;
            padding: 4px 8px;
            border-radius: 4px;
            font-weight: bold;
            font-size: 12px;

            .icon {
              width: 14px;
              height: 14px;
              margin-right: 4px;
            }

            &.release {
              background-color: #F3685F;
              color: #FFFFFF;
            }
          }
        }
      }
    }
  }
`
export const hide = css`
  display: none;
`
