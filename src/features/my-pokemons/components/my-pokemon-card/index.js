import { useState } from 'react'
import Image from 'next/image'
import Card from '@components/card'
import { useToast } from '@happyfresh/toast'
import ReleaseConfirmation from '../release-confirmation'
import useMyPokemons from '../../data/hooks'
import * as styles from './styles'

export default function MyPokemonCard ({ pokemon, index }) {
  const { releasePokemon } = useMyPokemons()
  const { openToast } = useToast()
  const [menuOpened, setMenuOpened] = useState(false)
  const [confirmRelease, setConfirmRelease] = useState(false)

  const handleMenuToggle = () => setMenuOpened(!menuOpened)
  const toggleReleaseConfirmation = () => setConfirmRelease(!confirmRelease)

  const handleReleaseClick = () => {
    handleMenuToggle()
    toggleReleaseConfirmation()
  }

  const handleConfirmRelease = () => {
    toggleReleaseConfirmation()
    releasePokemon({ index })
    openToast({
      messages: 'Pokemon has been released. :(',
      duration: 3000
    })
  }

  return (
    <Card flat css={styles.card}>
      <figure>
        <Image
          src={pokemon.image}
          width={80}
          height={80}
          alt={pokemon.nickName}
        />
      </figure>
      <div className='info'>
        <div className='nickname'>{pokemon.nickName}</div>
        <div className='name'>{pokemon.name}</div>
      </div>
      <div className='action'>
        <div className='wrapper'>
          <button className='menu-toggle' onClick={handleMenuToggle} />
          <ul css={[!menuOpened && styles.hide]}>
            <li>
              <button className='release' onClick={handleReleaseClick}>
                <div className='icon'>
                  <Image src={require('@images/icons/release.svg')} width={14} height={14} alt='Release' />
                </div>
                Release
              </button>
            </li>
          </ul>
        </div>
      </div>
      <ReleaseConfirmation
        pokemon={pokemon}
        open={confirmRelease}
        onDismiss={toggleReleaseConfirmation}
        onConfirm={handleConfirmRelease}
      />
    </Card>
  )
}
