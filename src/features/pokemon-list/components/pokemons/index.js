import { useEffect, useState } from 'react'
import Link from 'next/link'
import { useQuery } from '@apollo/client'
import Button from '@components/button'
import { PokemonsSection } from './styles'
import PokemonCard from '../pokemon-card'
import { GET_POKEMONS } from '../../queries'
import Loader from '../loader'

export default function Pokemons () {
  const [pokemons, setPokemons] = useState([])
  const [lastPage, setLastPage] = useState(1)
  const [loadingMore, setLoadingMore] = useState(false)
  const LIMIT = 10

  const { loading, data, fetchMore } = useQuery(GET_POKEMONS, {
    variables: {
      limit: LIMIT,
      offset: 0
    }
  })

  const showLoader = loading || loadingMore

  useEffect(() => {
    if (data) {
      setPokemons(data.pokemons.results)
    }
  }, [data])

  const handleFetchMore = () => {
    setLoadingMore(true)

    fetchMore({
      variables: {
        limit: LIMIT,
        offset: lastPage * LIMIT
      }
    }).then(response => {
      setLastPage(lastPage + 1)
      setPokemons([
        ...pokemons,
        ...response.data.pokemons.results
      ])
      setLoadingMore(false)
    })
  }

  return (
    <PokemonsSection>
      <ul>
        {pokemons.map(pokemon => (
          <li key={pokemon.id}>
            <Link href={`/${pokemon.name}`}>
              <a>
                <PokemonCard pokemon={pokemon} />
              </a>
            </Link>
          </li>
        ))}
      </ul>
      {showLoader && <Loader />}
      {!showLoader && <Button secondary block onClick={handleFetchMore}>Load more pokemon</Button>}
    </PokemonsSection>
  )
}
