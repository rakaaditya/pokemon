import styled from '@emotion/styled'

export const PokemonsSection = styled.section`
  padding-top: 16px;
  ul {
    list-style: none;
    padding: 0;
    padding-bottom: 40px;

    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;

    li {
      margin: 8px 0;
      cursor: pointer;

      a {
        text-decoration: none;
      }
    }
  }
`
