import ContentLoader from 'react-content-loader'

export default function Skeleton () {
  return (
    <ContentLoader width={164} height={195} viewBox='0 0 164 195'>
      <rect x={12} y={0} rx={4} ry={4} width={116} height={116} />
      <rect x={0} y={132} rx={4} ry={4} width={140} height={16} />
      <rect x={0} y={152} rx={4} ry={4} width={69} height={12} />
    </ContentLoader>
  )
}
