import { css } from '@emotion/react'
import styled from '@emotion/styled'

export const LoaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`
export const cardStyle = css`
  width: 164px;
  height: 195px;
`
