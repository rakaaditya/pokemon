import Card from '@components/card'
import Skeleton from './skeleton'
import { LoaderContainer, cardStyle } from './styles'

export default function Loader () {
  return (
    <LoaderContainer>
      <Card css={cardStyle}>
        <Skeleton />
      </Card>
      <Card css={cardStyle}>
        <Skeleton />
      </Card>
    </LoaderContainer>
  )
}
