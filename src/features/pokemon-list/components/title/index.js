import { H1 } from '@components/title'

export default function Title () {
  return <H1>What pokemon are you looking for?</H1>
}
