import Image from 'next/image'
import Card from '@components/card'
import { formatPokemonName } from '@utils/common'
import useMyPokemons from '@features/my-pokemons/data/hooks'

import * as styles from './styles'

export default function PokemonCard ({ pokemon }) {
  const { getOwnedCount } = useMyPokemons()
  const owned = getOwnedCount(pokemon.name)

  return (
    <Card css={styles.card}>
      <figure>
        <Image src={pokemon.dreamworld} width={100} height='100%' alt={pokemon.name} />
      </figure>
      <h3>{formatPokemonName(pokemon.name)}</h3>
      <span>{owned} owned</span>
    </Card>
  )
}
