import { css } from '@emotion/react'

export const card = css`
  width: 164px;
  padding-bottom: 16px;

  figure {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 8px;
  }

  h3 {
    font-weight: bold;
    color: #3E4148;
    font-size: 14px;
    margin-top: 12px;
  }

  span {
    display: block;
    color: #9DA3B4;
    font-size: 12px;
    font-weight: 500;
    margin-top: 4px;
  }
`
