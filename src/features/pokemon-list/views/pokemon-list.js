import dynamic from 'next/dynamic'
import Layout from '@components/layout'

const Title = dynamic(
  () => import('../components/title'),
  { ssr: false }
)
const Pokemons = dynamic(
  () => import('../components/pokemons'),
  { ssr: false }
)

export default function PokemonListView () {
  return (
    <Layout title='Home'>
      <Title />
      <Pokemons />
    </Layout>
  )
}
