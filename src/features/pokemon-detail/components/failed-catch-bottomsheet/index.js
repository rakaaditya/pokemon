import Image from 'next/image'
import { BottomSheet } from '@happyfresh/bottom-sheet'
import Button from '@components/button'
import { FailedContainer } from './styles'

export default function FailedCatchBottomSheet ({ open, onDismiss }) {
  return (
    <BottomSheet
      open={open} styles={{
        wrapper: {
          borderRadius: '16px 16px 0 0',
          height: 'unset',
          padding: '32px 16px 24px 16px'
        }
      }}
    >
      <FailedContainer>
        <figure>
          <Image src={require('@images/failed-catch.png')} width={100} height={100} alt='Failed!' />
        </figure>
        <div className='message'>Oh no! This pokemon has run away:(</div>
        <Button primary block onClick={onDismiss}>Try Again</Button>
      </FailedContainer>
    </BottomSheet>
  )
}
