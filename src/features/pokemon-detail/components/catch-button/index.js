import { useState } from 'react'
import Button from '@components/button'
import { formatPokemonName } from '@utils/common'
import SaveBottomSheet from '../save-bottomsheet'
import FailedCatchBottomSheet from '../failed-catch-bottomsheet'
import { CatchButtonContainer } from './styles'
import usePokemon from '../../data/hooks'

export default function CatchButton () {
  const [catchStatus, setCatchStatus] = useState(null)
  const { pokemon, loading } = usePokemon()

  if (loading) {
    return null
  }

  const handleCatch = () => {
    if (Math.random() < 0.5) {
      setCatchStatus('success')
    } else {
      setCatchStatus('failed')
    }
  }

  const handleDismiss = () => setCatchStatus(null)

  return (
    <CatchButtonContainer>
      <Button primary block onClick={handleCatch}>Catch {formatPokemonName(pokemon.name)}!</Button>
      <SaveBottomSheet open={catchStatus === 'success'} />
      <FailedCatchBottomSheet open={catchStatus === 'failed'} onDismiss={handleDismiss} />
    </CatchButtonContainer>
  )
}
