import styled from '@emotion/styled'

export const CatchButtonContainer = styled.div`
  position: fixed;
  width: 100%;
  left: 0;
  bottom: 0;
  padding: 16px;
  padding-bottom: 24px;
  background-color: #FFFFFF;
  box-shadow: 0px -2px 4px rgba(0, 0, 0, 0.05);
`
