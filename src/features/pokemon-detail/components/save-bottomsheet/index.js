import { useRef } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { BottomSheet } from '@happyfresh/bottom-sheet'
import { useToast } from '@happyfresh/toast'
import Input from '@components/input'
import Button from '@components/button'
import useMyPokemons from '@features/my-pokemons/data/hooks'
import usePokemon from '../../data/hooks'
import { SaveContainer } from './styles'

export default function SaveBottomSheet ({ open }) {
  const router = useRouter()
  const inputRef = useRef(null)
  const { pokemon } = usePokemon()
  const { pokemons, savePokemon } = useMyPokemons()
  const { openToast } = useToast()

  const handleSave = () => {
    const nickName = inputRef.current.value

    if (!nickName) {
      openToast({ messages: 'Pokemon name should not be empty', duration: 3000 })
      return false
    }

    const isNickNameExists = pokemons.find(pokemon => pokemon.nickName.toLowerCase() === nickName.toLowerCase())

    if (isNickNameExists) {
      openToast({ messages: `Whoops! You already have a pokemon with name ${nickName}!`, duration: 3000 })
      return false
    }

    savePokemon({
      name: pokemon.name,
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon.id}.svg`,
      nickName
    })

    return router.push('/my-pokemons')
  }

  return (
    <BottomSheet
      open={open} styles={{
        wrapper: {
          borderRadius: '16px 16px 0 0',
          height: 'unset',
          padding: '32px 16px 24px 16px'
        }
      }}
    >
      <SaveContainer>
        <figure>
          <Image src={require('@images/gotcha.png')} width={100} height={100} alt='Gotcha!' />
        </figure>
        <div className='message'>Yaaaay! You successfully catch this Pokemon!</div>
        <Input ref={inputRef} block placeholder='Name this pokemon...' />
        <Button primary block onClick={handleSave}>Save to My Pokemon</Button>
      </SaveContainer>
    </BottomSheet>
  )
}
