import styled from '@emotion/styled'

export const SaveContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .message {
    font-weight: bold;
    font-size: 20px;
    color: #3E4148;
    padding: 0 16px;
    padding-top: 16px;
    padding-bottom: 24px;
    text-align: center;
    line-height: 30px;
  }

  input {
    text-align: center;
    margin-bottom: 12px;
  }
`
