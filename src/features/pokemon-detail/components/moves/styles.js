import styled from '@emotion/styled'

export const MovesContainer = styled.div`
  .title {
    font-weight: 600;
    font-size: 12px;
    color: #9DA3B4;
    text-align: center;
  }

  ul {
    padding-top: 12px;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;

    li {
      flex-basis: 50%;
      padding: 4px 0;
      text-align: center;
      font-weight: bold;
      font-size: 14px;
      color: #3E4148;
    }
  }
`
