import Card from '@components/card'
import { formatAbilities } from '@utils/common'
import { MovesContainer } from './styles'
import usePokemon from '../../data/hooks'

export default function Moves () {
  const { pokemon, loading } = usePokemon()

  if (loading) {
    return null
  }

  return (
    <Card flat>
      <MovesContainer>
        <div className='title'>SPECIAL MOVES</div>
        <ul>
          {pokemon.moves.map((move, i) => (
            <li key={i}>{move.move.name}</li>
          ))}
        </ul>
      </MovesContainer>
    </Card>
  )
}
