import Card from '@components/card'
import { formatAbilities } from '@utils/common'
import { DetailContainer } from './styles'
import usePokemon from '../../data/hooks'

export default function Detail () {
  const { pokemon, loading } = usePokemon()

  if (loading) {
    return null
  }

  return (
    <Card flat>
      <DetailContainer>
        <div className='group'>
          <div className='property'>
            <div className='title'>Height</div>
            <div className='value'>{pokemon.height}</div>
          </div>
          <div className='property'>
            <div className='title'>Species</div>
            <div className='value'>{pokemon.species.name}</div>
          </div>
        </div>
        <div className='group'>
          <div className='property'>
            <div className='title'>Weight</div>
            <div className='value'>{pokemon.weight}</div>
          </div>
          <div className='property'>
            <div className='title'>Abilities</div>
            <div className='value'>{formatAbilities(pokemon.abilities)}</div>
          </div>
        </div>
      </DetailContainer>
    </Card>
  )
}
