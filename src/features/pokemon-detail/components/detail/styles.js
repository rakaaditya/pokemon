import styled from '@emotion/styled'

export const DetailContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 4px 0;

  .group {
    flex-basis: 100%;
    text-align: center;

    .property {
      &:last-child {
        margin-top: 16px;
      }

      .title {
        font-weight: 600;
        font-size: 12px;
        color: #9DA3B4;
        padding-bottom: 2px;
      }

      .value {
        font-weight: 700;
        font-size: 14px;
        color: #3E4148;
        padding-top: 2px;
      }
    }
  }
`
