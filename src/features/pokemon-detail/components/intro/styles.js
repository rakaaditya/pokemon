import styled from '@emotion/styled'

export const IntroContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  figure {
    margin-bottom: 24px;
  }
`
