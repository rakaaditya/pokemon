import Image from 'next/image'
import { H1 } from '@components/title'
import { formatPokemonName } from '@utils/common'
import { IntroContainer } from './styles'
import usePokemon from '../../data/hooks'

export default function Intro () {
  const { pokemon, loading } = usePokemon()

  if (loading) {
    return null
  }

  return (
    <IntroContainer>
      <figure>
        <Image src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon.id}.svg`} width={220} height={220} alt={pokemon.name} />
      </figure>
      <H1>{formatPokemonName(pokemon.name)}</H1>
    </IntroContainer>
  )
}
