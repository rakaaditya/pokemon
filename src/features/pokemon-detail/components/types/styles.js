import styled from '@emotion/styled'

export const TypesContainer = styled.div`
  .title {
    font-weight: 600;
    font-size: 12px;
    color: #9DA3B4;
    text-align: center;
  }

  .types {
    margin-top: 4px;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
      margin: 0 2px;
      padding: 4px 8px;
      border-radius: 2px;
      font-weight: bold;
      font-size: 12px;
    }
  }
`
