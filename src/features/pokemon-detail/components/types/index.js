import { TypesContainer } from './styles'
import usePokemon from '../../data/hooks'
import { TYPE_COLOR } from '../../constants'

export default function Types () {
  const { pokemon, loading } = usePokemon()

  if (loading) {
    return null
  }

  return (
    <TypesContainer>
      <div className='title'>TYPE</div>
      <div className='types'>
        {pokemon.types.map((type, i) => (
          <span key={i} style={{ ...TYPE_COLOR[type.type.name] }}>{type.type.name}</span>
        ))}
      </div>
    </TypesContainer>
  )
}
