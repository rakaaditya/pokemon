import { useContext } from 'react'
import Context from './context'

export default function PokemonHooks () {
  const ctx = useContext(Context)

  return ctx
}
