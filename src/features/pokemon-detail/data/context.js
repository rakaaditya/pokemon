import React from 'react'

export default React.createContext({
  pokemon: null,
  loading: false
})
