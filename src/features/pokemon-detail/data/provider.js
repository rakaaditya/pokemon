import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import client from '@config/apollo-client'
import Context from './context'
import { GET_POKEMON } from './queries'

export default function PokemonDetailProvider ({ children }) {
  const { query: { pokemonName } } = useRouter()
  const [pokemon, setPokemon] = useState({})
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    if (pokemonName) {
      client.query({
        query: GET_POKEMON,
        variables: {
          name: pokemonName
        }
      }).then(response => {
        setPokemon(response.data.pokemon)
        setLoading(false)
      })
    }
  }, [pokemonName])

  return (
    <Context.Provider value={{
      pokemon,
      loading
    }}
    >
      {children}
    </Context.Provider>
  )
}
