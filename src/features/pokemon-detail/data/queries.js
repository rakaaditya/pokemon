import { gql } from '@apollo/client'

export const GET_POKEMON = gql`
  query getPokemon($name: String!) {
    pokemon(name: $name) {
      id
      name
      height
      weight
      types {
        type {
          name
        }
      }
      species {
        name
      }
      abilities {
        is_hidden
        ability {
          name
        }
      }
      moves {
        move {
          name
        }
      }
    }
  }
`
