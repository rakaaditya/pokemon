import dynamic from 'next/dynamic'
import Layout from '@components/layout'
import { formatPokemonName } from '@utils/common'
import usePokemon from '../data/hooks'

const Intro = dynamic(
  () => import('../components/intro'),
  { ssr: false }
)

const Types = dynamic(
  () => import('../components/types'),
  { ssr: false }
)

const Detail = dynamic(
  () => import('../components/detail'),
  { ssr: false }
)

const Moves = dynamic(
  () => import('../components/moves'),
  { ssr: false }
)

const CatchButton = dynamic(
  () => import('../components/catch-button'),
  { ssr: false }
)

export default function PokemonDetailView () {
  const { pokemon } = usePokemon()
  return (
    <Layout title={formatPokemonName(pokemon.name || 'Pokemon Detail')}>
      <Intro />
      <section style={{ marginTop: 8 }}>
        <Types />
      </section>
      <section style={{ marginTop: 24 }}>
        <Detail />
      </section>
      <section style={{ marginTop: 12 }}>
        <Moves />
      </section>
      <section style={{ marginBottom: 106 }}>
        <CatchButton />
      </section>
    </Layout>
  )
}
