export const TYPE_COLOR = {
  normal: {
    backgroundColor: '#F5F5F7',
    color: '#9DA3B4'
  },
  fighting: {
    backgroundColor: '#FDECEB',
    color: '#F3685F'
  },
  flying: {
    backgroundColor: '#FEF4E8',
    color: '#F9AE4C'
  },
  poison: {
    backgroundColor: '#DFFFE8',
    color: '#3CDB69'
  },
  ground: {
    backgroundColor: '#FFEBC5',
    color: '#AA7100'
  },
  rock: {
    backgroundColor: '#F0F0F0',
    color: '#696969'
  },
  bug: {
    backgroundColor: '#FDEEE9',
    color: '#F47C55'
  },
  ghost: {
    backgroundColor: '#F0F0F0',
    color: '#696969'
  },
  steel: {
    backgroundColor: '#E7EDFF',
    color: '#5A82F4'
  },
  fire: {
    backgroundColor: '#FDECEB',
    color: '#F3685F'
  },
  water: {
    backgroundColor: '#EBF5F7',
    color: '#62B6C3'
  },
  grass: {
    backgroundColor: '#F3F9EB',
    color: '#A3D166'
  },
  electric: {
    backgroundColor: '#E7EDFF',
    color: '#5A82F4'
  },
  psychic: {
    backgroundColor: '#EDEBF4',
    color: '#7061A9'
  },
  ice: {
    backgroundColor: '#EBF5F7',
    color: '#62B6C3'
  },
  dragon: {
    backgroundColor: '#FDEEE9',
    color: '#F47C55'
  },
  dark: {
    backgroundColor: '#F0F0F0',
    color: '#696969'
  },
  fairy: {
    backgroundColor: '#FDEFF4',
    color: '#F386A8'
  },
  unknown: {
    backgroundColor: '#F5F5F7',
    color: '#9DA3B4'
  },
  shadow: {
    backgroundColor: '#EDEBF4',
    color: '#7061A9'
  }
}
